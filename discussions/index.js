/* A.
I. [SECTION] ARRAY

*/

// an array in programming is simply a list of data( or list of item).

// They are declared using square bracket ([]) also known as "Array Literals".

// Commonly used to store numerous amount of data to manipulate inorder to perform a number of task.

/*
for the Syntax of array is(either if it is a let of const:


	-Syntax:
	let/const arrayName = [elementA, elementB, elementC...];

*/

/*
[Square brackets means it is an array.]

*/

// for example:

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']

console.log(studentNumbers);

/*-------------------------------*/
//II.
 // Common example of arrays.

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of an array but some of this is not recommended

let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

/*--------------------------------------------------------------------------*/
// III.
// Alternative way to write arrays.

let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];

// IV.
//Creating an array with values from variables:

// If mag lalagay kayo ng variable, wag nyo lagyan ng quotation.

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1,city2,city3];

console.log(myTasks);
console.log(cities);

/*------------------------------------------------------------------------*/

/*B.
	[SECTION] .length property
*/

// I.
// The .length property allows us to get and set the total numbers of items in an array.


// Example:
console.log(myTasks.length);
console.log(cities.length);
// .length is kina,count niya yung nasa console.log(myTasks.length); at console.log(cities.length);


// II.
let blankArr = [];
console.log(blankArr.length);

// III.
// another example: try natin mag,bura.
// Kung sino yung pinaka,dulo. siya yung mabubura kapag nag,-1 tayo.
// Deleting array using .length

// for example:
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);


// pwedeng sa cities or sa decrement na --

// for example:
cities.length--;
console.log(cities);


// IV.
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++
console.log(theBeatles);

// so parehas lang ung -1 and -- sir?
// yes, equivalent sila... x=x-1; x--; x-=1;





/*---------------------------------------------------------------------*/
// C.
// [SECTION] Reading from Arrays
// Accessing array elements is one of the more common taks that we do with an array. This can be done through the use of array indexes.

// This is Array indexes.


// I.
/*
for example:

	SYNTAX:
		arrayName[index];


*/
// this formula is targeted in grades. You can see the notes if you scrolled it up.
console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]);
// for the grades, kapag sumobra, elalagay sa console is undefined.



// II.
// Let us try to re,assign using array indexes.

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]

console.log('Array before reassignment');
console.log(lakersLegends);

// gusto ko sana palitan c Lebron into Pau Gasol
// example:
lakersLegends[2] = "Paul Gasol";
console.log('Array after reassignment');
console.log(lakersLegends);

/*--------------------------------------------------------------------*/
/*

// [SECTION] Reading from Arrays
// Accessing array elements is one of the more common tasks that we do with an array. This can be done through the use of array indexes.

/*
 	SYNTAX:
 		arrayName[index];

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]

console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";

*/

//Adding an Array

let newArr = [];
console.log(newArr[0]);






// III.
//newArr[0] is undefined because we haven't yet defined the item/data for that index, we can update the index with a new value: (array is mag,bawas, mag-dag2x)

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

// newArr.push("Food");
// console.log(newArr);

// for loops in array

for (let index = 0; index < newArr.length; index++) {

	console.log(newArr[index]);
}


// IV.
// For loops and if else in an array

// Given an array of numbers, you can also show if the following items in the array is divisible by 5 or not. You can mix in an if-else statements in the loop:

// example:
let numArr = [5,12,30,46,40];

for(let index = 0; index < numArr.length; index++){

    if(numArr[index] % 5 === 0){

        console.log(numArr[index] + " is divisible by 5");

    } else {

        console.log(numArr[index] + " is not divisible by 5");

    }

}
/*-------------------------------------------------------------------*/
// D.
// [SECTION] Multidimentional Arrays

/*
	- Multidimentional arrays are useful for strong complex data structures.
	- A practical application of this is to help visualize or to create real world objects.
	- Through useful in a number of cases, creating complex array structures is not always recommended.

*/

// how to count in chessboard on rows and also in columns? it is always starts wih the zero count, so a1 is counted as 0 then 1, 2, 3 and so on.

// for example:
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

// Accessing elements of a multidimentional arrays.
console.log(chessBoard[1][4]);

console.log("Pawn moves to: " + chessBoard[1][5]);

// para kasing machine learning training na if move dito, iwas ka don, etc.
